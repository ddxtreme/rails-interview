require "rails_helper"

RSpec.describe 'Api requests', type: :request do

  describe 'api key' do
    context 'invalid' do
      it 'should return unauthorized' do
        get "/api/v1/questions", {}, {'HTTP_ACCEPT' => 'application/json'}
        expect(response.status).to eq(401)
        expect(response.headers).to include('WWW-Authenticate' => 'Token realm=Application')
      end
    end

    context 'valid' do
      it 'should proceed' do
        tenant = FactoryGirl.create(:tenant)

        get "/api/v1/questions", {}, {
          'HTTP_ACCEPT' => 'application/json',
          'Authorization' => "Token token=#{tenant.api_key}"
        }
        expect(response.status).to eq(200)
      end
    end
  end

  describe 'requests count' do
    let(:tenant) { FactoryGirl.create(:tenant) }

    before :each do
      TenantRequestTracker.new(tenant: tenant).clear!
    end

    it 'should track requests count' do
      get "/api/v1/questions", {}, {
        'HTTP_ACCEPT' => 'application/json',
        'Authorization' => "Token token=#{tenant.api_key}"
      }
      get "/api/v1/questions", {}, {
        'HTTP_ACCEPT' => 'application/json',
        'Authorization' => "Token token=#{tenant.api_key}"
      }
      tenant_2 = FactoryGirl.create(:tenant)
      get "/api/v1/questions", {}, {
        'HTTP_ACCEPT' => 'application/json',
        'Authorization' => "Token token=#{tenant_2.api_key}"
      }
      tracker = TenantRequestTracker.new(tenant: tenant)
      expect(tracker.current_count).to eq(2)
    end
  end
end