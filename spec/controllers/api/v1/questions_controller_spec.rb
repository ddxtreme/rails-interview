require 'rails_helper'

RSpec.describe Api::V1::QuestionsController, type: :controller do

  before :each do
    request.env["HTTP_ACCEPT"] = 'application/json'
    request.headers.merge!('Authorization' => "Token token=#{tenant.api_key}")
  end

  let(:tenant) { FactoryGirl.create(:tenant) }

  describe "GET #index" do
    it_behaves_like 'a successful request'

    it "return an empty array when no questions" do
      get :index
      expect(response_json).to eq([])
    end

    it "return an array of questions" do
      FactoryGirl.create(:question, :with_answers)

      get :index

      expected_json = ActiveModelSerializers::SerializableResource.new(
        Question.visible,
        include: ['user', 'answers.user']
      ).to_json

      expect(response.body).to eq(expected_json)
    end

    it "return an array without private questions" do
      FactoryGirl.create(:question, :private, :with_answers)

      get :index

      expect(response_json).to eq([])
    end

    it "return an array with question user id and user name" do
      FactoryGirl.create(:question, :with_answers)
      get :index
      expect(response_json[0]['user'].keys).to eq(['id', 'name'])
    end

    it 'return an array of answers' do
      FactoryGirl.create(:question, :with_answers)
      get :index

      expect(response_json[0].keys).to include('answers')
      expect(response_json[0]['answers'].count).to eq(1)
    end

    it "return an array with answers user id and user name" do
      FactoryGirl.create(:question, :with_answers)
      get :index

      first_answer = response_json[0]['answers'][0]

      expect(first_answer['user'].keys).to eq(['id', 'name'])
    end
  end

end
