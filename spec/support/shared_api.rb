shared_context 'a successful request' do
  it 'returns 200 status code' do
    expect(response).to have_http_status(:ok)
  end
end