FactoryGirl.define do
  factory :question do
    title { FFaker::HipsterIpsum.sentence }
    user

    trait :private do
      private true
    end

    trait :with_answers do
      transient { number_of_answers 1 }
      after(:create) do |question, evaluator|
        create_list(:answer, evaluator.number_of_answers, question: question)
      end
    end
  end
end