FactoryGirl.define do
  factory :answer do
    question
    user
    body { FFaker::HipsterIpsum.sentence }
  end
end