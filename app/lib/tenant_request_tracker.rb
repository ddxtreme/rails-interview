class TenantRequestTracker
  attr_accessor :tenant

  def initialize tenant:
    @tenant = tenant
  end

  def track!
    cache.set(key, current_count + 1)
  rescue
    false
  end

  def clear!
    cache.del(key)
  rescue
    false
  end

  def current_count
    cache_get.to_i 
  rescue 1
  end

  private

    def key
      "requests:#{tenant.id}"
    end

    def cache_get
      cache.get(key)
    rescue false
    end

    def cache
      $redis
    end
end