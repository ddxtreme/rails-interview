module Api::V1
  class BaseController < ApplicationController
    include Api::V1::Concerns::Authenticator
    include Api::V1::Concerns::RequestTracker
    protect_from_forgery with: :null_session

    before_action :check_format
    before_action :authenticate_user!
    before_action :track_request!

    private

      def check_format
        render nothing: true, status: 406 unless params[:format] == 'json' || request.headers["Accept"] =~ /json/
      end
  end
end