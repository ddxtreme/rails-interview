module Api::V1::Concerns::Authenticator
  extend ActiveSupport::Concern

  included do
    attr_reader :current_tenant

    private
      def unauthenticated!
        response.headers['WWW-Authenticate'] = "Token realm=Application"
        render json: {}, status: 401
      end

      def authenticate_user!
        api_key = fetch_token
        tenant = secure_fetch_tenant(fetch_token)

        return unauthenticated! if tenant.nil?

        @current_tenant = tenant
      end

      def fetch_token
        token, = ActionController::HttpAuthentication::Token
                 .token_and_options(request)
        token || ""
      end

      def secure_fetch_tenant token
        tenant = Tenant.find_by(api_key: token)
        token_digest = Digest::SHA256.hexdigest(token)
        tenant_token_digest = tenant ? Digest::SHA256.hexdigest(tenant.api_key) : ""

        ActiveSupport::SecurityUtils.secure_compare(token_digest, tenant_token_digest)
        tenant
      end
  end
end