module Api::V1::Concerns::RequestTracker
  extend ActiveSupport::Concern

  included do

    private
      def track_request!
        return false unless current_tenant
        TenantRequestTracker.new(tenant: current_tenant).track!
      end
  end
end