class AnswerSerializer < ActiveModel::Serializer
  attribute :body
  belongs_to :user
end