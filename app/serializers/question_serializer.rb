class QuestionSerializer < ActiveModel::Serializer
  attribute :title
  belongs_to :user
  has_many :answers
end